{{-- s02 Activity Solution --}}

@extends('layouts.app')

@section('content')

<div class="text-center p-5">

    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid mb-3" width="800">

    <h1 class="mb-3 mt-3">Featured Posts:</h1>
    @if(count($posts) > 0)
    @foreach($posts as $post)
        <div class="card text-center mb-4">
            <div class="card-body">
                <h4 class="card-title mb-3">
                    <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                </h4>
                <h6 class="card-text mb-3">
                    Author: {{$post->user->name}}
                </h6>
            </div>
        </div>
    @endforeach
    @endif

    
</div>



    

@endsection