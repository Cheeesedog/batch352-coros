@extends('layouts.app')

@section('content')

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created At: {{$post->mcreated_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif

			@if(Auth::user())
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
					@csrf
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					  Comment
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
					        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					      </div>
					      <div class="modal-body">
					        <label for="content">Comment:</label>
							<textarea class="form-control" id="content" name="content" rows="3"></textarea>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-primary">Comment</button>
					      </div>
					    </div>
					  </div>
					</div>
				</form>
			@endif


				

			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>

	@if(count($postComments) > 0)
	<h3 class="mt-5">Comment:</h3>

		@foreach ($postComments as $comment)
		    <div class="card mt-3">
		        <div class="card-body text-center mt-3">
		            <h4 class="card-text">
		                {{ $comment->content }}
		            </h4>
		        </div>
		        <div class="ms-auto me-3">
			        <h6 class="card-text mb-3">
			                Posted by: {{ $comment->user->name }}
		            </h6>
		            <p class="card-subtitle mb-3 text-muted">
		                Posted on: {{ $comment->created_at }}
		            </p>
		        </div>
		        
		        	
		    </div>
		@endforeach
	@endif

@endsection