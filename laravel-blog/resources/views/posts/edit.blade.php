@extends('layouts.app')

@section('content')
	{{-- 
	<div>
		<form method="POST" action="/post/{{$post->id}}">

		@method('PUT')
		
		<label>Title:</label>
		<input type="text" name="title" value="{{$post->title}}" class="form-control" />
		<label>Content:</label>
		<textarea name="content" rows="3" class="form-control mb-2">{{$post->content}}</textarea>
		<button type="submit" class="btn btn-primary">Update Post</button>
		</form>
		
	</div> --}}

	<form method="POST" action="/posts/{{$post->id}}">
		@method('PUT')
		@csrf
		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" name="title" id="title" class="form-control" value="{{$post->title}}" />
		</div>
		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" id="content" name="content" rows="3">{{$post->content}}</textarea>
		</div>
		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Update Post</button>
		</div>
	</form>
@endsection